import express from "express";
import { graphqlHTTP } from "express-graphql";
import { makeExecutableSchema } from "graphql-tools";
import { ProductsService } from "./products/products.service.js";
import { UsersService } from "./users/users.service.js";

const app = express();
const port = 3000;

let typeDefs = [
  `
    type Query {
        hello: String
    }

    type Mutation {
        hello(message: String): String
    }
`,
];

// hello("hello")
let resolvers = {
  Query: {
    hello: () => "Hello world from server!",
  },
  Mutation: {
    hello: (_, helloData) => {
      return helloData.message + " from server";
    },
  },
};

// Services
let productsService = new ProductsService();
let userService = new UsersService();

typeDefs += productsService.configTypeDefs();
typeDefs += userService.configTypeDefs();

productsService.configResolvers(resolvers);
userService.configResolvers(resolvers);

app.use(
  "/graphql",
  graphqlHTTP({
    schema: makeExecutableSchema({ typeDefs, resolvers }),
    graphiql: true,
  })
);

app.listen(port);
