export class User {
  id = 0;
  firstName = "";
  lastName = "";
  email = "";
  password = "";
  permissionLevel = 1;

  constructor(id, firstName, lastName, email, password, permissionLevel) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.permissionLevel = permissionLevel;
  }
}
