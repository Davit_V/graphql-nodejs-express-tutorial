export class UsersService {
  users = [];

  configTypeDefs() {
    let typeDefs = `
              type User {
                firstName: String,
                lastName: String,
                id: Int,
                password: String,
                permissionLevel: Int,
                email: String
              } `;
    typeDefs += ` 
              extend type Query {
              users: [User]
            }
            `;

    typeDefs += `
              extend type Mutation {
                user(firstName:String,
                 lastName: String,
                 password: String,
                 permissionLevel: Int,
                 email: String,
                 id:Int): User!
              }`;
    return typeDefs;
  }

  configResolvers(resolvers) {
    resolvers.Query.users = () => {
      return this.users;
    };
    resolvers.Mutation.user = (_, user) => {
      user.password = user.password + "hashed";
      this.users.push(user);
      return user;
    };
  }
}
