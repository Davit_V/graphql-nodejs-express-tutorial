export class Product {
  id = 0;
  name = "";
  description = "";
  price = 0;

  constructor(productId, productName, productDescription, price) {
    this.id = productId;
    this.name = productName;
    this.description = productDescription;
    this.price = price;
  }
}
