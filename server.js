import express from "express";
import { graphqlHTTP } from "express-graphql";
import { makeExecutableSchema } from "graphql-tools";

const app = express();
const port = 3000;

let typeDefs = [
  `
    type Query {
        hello: String
    }

    type Mutation {
        hello(message: String): String
    }
`,
];

// hello("hello")
let resolvers = {
  Query: {
    hello: () => "Hello world from server!",
  },
  Mutation: {
    hello: (_, helloData) => {
      return helloData.message + " from server";
    },
  },
};

app.use(
  "/graphql",
  graphqlHTTP({
    schema: makeExecutableSchema({ typeDefs, resolvers }),
    graphiql: true,
  })
);

app.listen(port);
